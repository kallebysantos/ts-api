import { v4 as uuid } from 'uuid';

export interface IUser {
  _id: string,
  name: string,
  email: string,
  password: string,
}

export const User = (props: Omit<IUser, '_id'>, id?: string): IUser => ({
  ...props,
  _id: (id) || uuid(),
});
