import MongooseProvider from '@api/providers/MongooseProvider';

const database = MongooseProvider;

export default database;
