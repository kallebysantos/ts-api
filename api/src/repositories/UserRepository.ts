import { Document } from 'mongoose';
import { IUser } from '@api/entities/User';
import UserModel from '@api/models/UserModel';

export interface IUserRepository {
  parse(raw: Document): Promise<IUser>;
  save(user: IUser): Promise<IUser>;
  findByEmail(email: string): Promise<IUser | null>;
}

export const UserRepository = (): IUserRepository => ({
  async parse(raw: Document): Promise<IUser> {
    return ({
      _id: raw.get('id'),
      name: raw.get('name'),
      email: raw.get('email'),
      password: raw.get('password'),
    });
  },
  async save(user: IUser): Promise<IUser> {
    const model = new UserModel(user);
    const created = await model.save();
    return this.parse(created);
  },
  async findByEmail(email: string): Promise<IUser | null> {
    const user = await UserModel.findOne({ email });
    return (user === null) ? null : this.parse(user);
  },
});
