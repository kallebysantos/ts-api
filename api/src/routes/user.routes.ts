import { ServerRoute } from '@hapi/hapi';
import CreateUserUCRoutes from '@api/usecases/CreateUser/CreateUserUC.routes';

const userRoutes: ServerRoute[] = [
  CreateUserUCRoutes,
];

export default userRoutes;
