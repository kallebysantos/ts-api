import dotenv from 'dotenv';

import app from '@api/app';
import database from '@api/index';
import { IDatabaseProvider } from '@api/i-providers/IDatabaseProvider';

dotenv.config();
const databaseProvider: IDatabaseProvider = database;

const uri = databaseProvider.generateUri({
  host: process.env.DB_HOST,
  port: process.env.DB_PORT,
  user: process.env.DB_USER,
  password: process.env.DB_PASSWORD,
  database: process.env.DB_DATABASE,
});

databaseProvider.connect(uri);

app.start();
