import { Util } from '@hapi/hapi';

export interface ITestRouter {
  method: Util.HTTP_METHODS_PARTIAL,
  url: string,
  payload: Record<string, unknown>,
  headers: {
    authorization: string,
  }
}

export const TestRoute = (props: Omit<ITestRouter, 'headers'>, auth?: string): ITestRouter => ({
  ...props,
  headers: {
    authorization: (`Bearer ${auth}`) || '',
  },
});

export default TestRoute;
