import { IUser, User } from '@api/entities/User';
import { IUserRepository } from '@api/repositories/UserRepository';
import { ErrorTypes } from '@api/utils/ErrorMessenger';

import { ICreateUserResquestDTO } from './CreateUserUC.dto';

export interface ICreateUser {
  execute(data: ICreateUserResquestDTO): Promise<IUser>,
}

export const CreateUser = (Repository: IUserRepository): ICreateUser => ({
  async execute(data: ICreateUserResquestDTO): Promise<IUser> {
    const userAlreadyExists = await Repository.findByEmail(data.email);

    if (userAlreadyExists) {
      throw ErrorTypes.ALREADY_EXISTS;
    }

    const user = User(data);
    return Repository.save(user);
  },
});
