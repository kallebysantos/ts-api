import App from '@api/app';
import It from '@api/tests/TestSetup.spec';
import TestRoute from '@api/tests/TestRouter.spec';

const request = TestRoute({
  url: '/user',
  method: 'POST',
  payload: {
    name: 'User Example',
    email: 'user@example.com',
    password: 'user123',
  },
});

It('Should send an empty payload', (expect) => {
  const req = TestRoute({
    url: '/user',
    method: 'POST',
    payload: {},
  });

  return App.inject(req)
    .then((res) => expect.is(res.statusCode, 400));
});

It('Should send an invalid data payload', (expect) => {
  const req = TestRoute({
    url: '/user',
    method: 'POST',
    payload: {
      name: '12',
      email: 'some.thing.com',
      password: '123',
    },
  });

  return App.inject(req)
    .then((res) => expect.is(res.statusCode, 400));
});

It.serial('Should create a valid user', (expect) => (
  App.inject(request)
    .then((res) => expect.is(res.statusCode, 201))
));

It.serial('Should send an already in use email', (expect) => (
  App.inject(request)
    .then((res) => {
      expect.is(res.statusCode, 422);
    })
));
