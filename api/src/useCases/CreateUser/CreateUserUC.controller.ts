import { ErrorResponse } from '@api/utils/ErrorMessenger';
import { Request, ResponseToolkit } from '@hapi/hapi';
import { ICreateUser } from './CreateUser';
import { ICreateUserRequest } from './CreateUserUC.schema';

export interface ICreateUserController {
  handler(req: Request, res: ResponseToolkit): Promise<unknown>,
}

export const CreateUserController = (createUser: ICreateUser): ICreateUserController => ({
  async handler(req: ICreateUserRequest, res: ResponseToolkit): Promise<unknown> {
    const { name, email, password } = (req.payload);

    return createUser.execute({ name, email, password })
      .then((result) => res.response(result).code(201))
      .catch((error) => ErrorResponse(error));
  },
});

export default CreateUserController;
