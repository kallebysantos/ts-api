import { Request, ResponseToolkit, ServerRoute } from '@hapi/hapi';
import { createUserController } from '.';
import { RouteSchema } from './CreateUserUC.schema';

const CreateUser = (): ServerRoute => ({
  method: 'POST',
  path: '/user',
  handler: (req: Request, res: ResponseToolkit) => (
    createUserController.handler(req, res)
  ),
  options: {
    validate: {
      payload: RouteSchema,
    },
    description: 'Route for new user creation',
  },
});

export default CreateUser();
