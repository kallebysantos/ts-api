import Joi, { Schema } from 'joi';

import Hapi from '@hapi/hapi';

export interface ICreateUserRequest extends Hapi.Request {
  payload: {
    name: string,
    email: string,
    password: string,
  };
}

export const RouteSchema: Schema = Joi.object({
  name: Joi.string().min(3).required(),
  email: Joi.string().email().required(),
  password: Joi.string().min(5).required(),
});
