import It from '@api/tests/TestSetup.spec';
import UserModel from '@api/models/UserModel';
import { UserRepository } from '@api/repositories/UserRepository';

import { CreateUser } from './CreateUser';
import { ICreateUserResquestDTO } from './CreateUserUC.dto';

It.afterEach.always('Cleaning database', async () => {
  await UserModel.deleteMany();
});

It('Should create user', async (expect) => {
  const userData: ICreateUserResquestDTO = {
    name: 'Kalleby Santos',
    email: 'kalleby.santos@gmail.com',
    password: 'kaka123',
  };

  await CreateUser(UserRepository()).execute(userData);

  const createdUser = await UserRepository().findByEmail(userData.email);
  return expect.is(userData.email, createdUser?.email);
});
