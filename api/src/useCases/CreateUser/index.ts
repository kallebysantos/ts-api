import { UserRepository } from '@api/repositories/UserRepository';
import { CreateUser } from './CreateUser';
import CreateUserController from './CreateUserUC.controller';

const userRepository = UserRepository();
const createUser = CreateUser(userRepository);
const createUserController = CreateUserController(createUser);

export {
  createUser,
  createUserController,
};
