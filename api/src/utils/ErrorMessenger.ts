import boom from '@hapi/boom';

export const ErrorTypes = {
  INVALID_VALUES: boom.badData('Invalid values'),
  ALREADY_EXISTS: boom.badData('Already exists'),
  DATA_NOT_FOUND: boom.badData('This record does not exists'),
  INVALID_QUERY: boom.badRequest('Invalid query parammeter'),
  INVALID_CREDENTIALS: boom.badData('Invalid credentials'),
  INTERNAL_ERROR: boom.badImplementation('Undefined error'),
};

export const ErrorResponse = (error: Error, showLogs?: boolean): boom.Boom<unknown> => {
  if (showLogs) {
    console.error(error);
  }
  return (boom.isBoom(error)) ? error : boom.badImplementation();
};
