#!/bin/bash
STATUS=''

LIGHT_CYAN='\033[1;36m'
CYAN='\033[0;36m'
GREEN='\033[1;32m'
PURPLE='\033[1;33m' 
NC='\033[0m' # No Color

function verify {
  case $OPTION in
  'start') start;;
  'test') test;;
  'dev') dev;;
  'test:watch') watch;;
  *) start ;;
  esac
}

function start {
  STATUS='Production'
  echo -e "${GREEN}Installing dependencies...${NC}"
  echo -e "${PURPLE}Starting server in ${STATUS} mode...${NC}"
  yarn install 
  yarn start
}

function dev {
  STATUS='Developing'
  echo -e "${PURPLE}Starting server in ${STATUS} mode...${NC}"
  yarn dev --silent --color
}

function test {
  yarn test --silent --color
}

function watch {
  DEBUG=ava:watcher yarn test --silent --color --watch --verbose
}

OPTION=$1
clear
verify
